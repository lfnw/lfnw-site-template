var ready = (callback) => {
  if (document.readyState != "loading") callback();
  else document.addEventListener("DOMContentLoaded", callback);
}

ready(() => {
  /* Do things after DOM has fully loaded */
  console.log("Ready...");

  document.querySelectorAll("a.open-modal").forEach(node => {
    node.addEventListener("click", (e) => {
      document.getElementById(e.currentTarget.dataset.modal).showModal();
      e.preventDefault();
    });
  });

  document.querySelectorAll("a.close-modal").forEach(node => {
    node.addEventListener("click", (e) => {
      e.currentTarget.closest('dialog').close();
      e.preventDefault();
    });
  });

  document.querySelectorAll("dialog").forEach(node => {
    /* pretend dialog tags work in older browsers */
    dialogPolyfill.registerDialog(node);
    /* clicking outside the dialog closes it */
    node.addEventListener("click", (e) => {
      var rect = node.getBoundingClientRect();
      var isInDialog = (rect.top <= e.clientY && e.clientY <= rect.top + rect.height &&
        rect.left <= e.clientX && e.clientX <= rect.left + rect.width);
      if (!isInDialog) {
        node.close();
      }
    });
  });
});
